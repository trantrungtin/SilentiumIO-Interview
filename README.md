# Problem
Write a program that prints the numbers from 1 to n. But for multiples of four print "Silentium" instead of the number and for the multiples of seven print "IO". For numbers which are multiples of both four and seven print "SilentiumIO"

# Run 
`./gradlew run`

### Example
```shell script
./gradlew run

> Task :run
Enter your number:
<=<=========----> 75% EXECUTING [8s]
Result:
1
2
3
Silentium
5
6
IO
Silentium
9
10
```

# Test
`./gradlew clean test`

### Example
```shell script
./gradlew clean test

> Task :test

io.silentium.IOHelperTest > getConsoleNumberTest PASSED

io.silentium.IOHelperTest > printIntTest PASSED

io.silentium.IOHelperTest > printStringTest PASSED

io.silentium.ProblemTest > printMagicNumberWithInvalidArgumentTest PASSED

io.silentium.ProblemTest > printMagicNumberTest PASSED

io.silentium.ProblemTest > printMagicNumberMultiples4Test PASSED

io.silentium.ProblemTest > printMagicNumberMultiples7Test PASSED

io.silentium.ProblemTest > printMagicNumberMultiples4And7Test PASSED
```