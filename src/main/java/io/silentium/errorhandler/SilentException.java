package io.silentium.errorhandler;

public class SilentException extends Exception {
    private final SilentError mError;
    public SilentException(SilentError error) {
        super("(" + error +")");
        mError = error;
    }

    public SilentError getError() {
        return mError;
    }
}
