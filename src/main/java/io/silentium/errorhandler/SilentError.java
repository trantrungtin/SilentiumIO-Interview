package io.silentium.errorhandler;

public enum SilentError {
    ERR_INVALID_PARAMETER,
    ERR_READ_INPUT,
}
