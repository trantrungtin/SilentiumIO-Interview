package io.silentium;

import io.silentium.errorhandler.SilentError;
import io.silentium.errorhandler.SilentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Write a program that prints the numbers from 1 to n. But for multiples of four print "Silentium"
 * instead of the number and for the multiples of seven print "IO". For numbers which are multiples
 * of both four and seven print "SilentiumIO".
 */

public class Problem {
    private final Logger log = LoggerFactory.getLogger(getClass());
    private IOHelper mIOHelper;
    public Problem(IOHelper ioHelper) {
        mIOHelper = ioHelper;
    }

    public void printMagicNumber(int number) throws SilentException {
        if (number < 1) {
            log.error("number should be greater than 0: {}", number);
            throw new SilentException(SilentError.ERR_INVALID_PARAMETER);
        }

        for (int value = 1; value <= number; value++) {
            // multiples of both four and seven
            if (value % 28 == 0) {
                mIOHelper.printString("SilentiumIO");
            } else if (value % 7 == 0) {
                mIOHelper.printString("IO");
            } else if (value % 4 == 0) {
                mIOHelper.printString("Silentium");
            } else {
                mIOHelper.printInt(value);
            }
        }
    }
}
