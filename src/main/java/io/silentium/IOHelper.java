package io.silentium;

import io.silentium.errorhandler.SilentError;
import io.silentium.errorhandler.SilentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

public class IOHelper {
    private final Logger log = LoggerFactory.getLogger(IOHelper.class);
    private Scanner mScanner;
    public IOHelper() {
        mScanner = new Scanner(System.in);
    }

    public int getConsoleNumber() throws SilentException {
        try {
            return mScanner.nextInt();
        } catch (Exception e) {
            log.error("Could not get a console number ", e);
            throw new SilentException(SilentError.ERR_READ_INPUT);
        }
    }

    public void printInt(int value) {
        System.out.println(value);
    }

    public void printString(String value) {
        System.out.println(value);
    }
}
