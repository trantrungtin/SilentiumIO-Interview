package io.silentium;

import io.silentium.errorhandler.SilentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String ignored[]) {
        final IOHelper ioHelper = new IOHelper();
        Problem problem = new Problem(ioHelper);
        try {
            System.out.println("Enter your number: ");
            int number = ioHelper.getConsoleNumber();
            System.out.println("Result: ");
            problem.printMagicNumber(number);
        } catch (SilentException e) {
            log.error("Error: {}", e.getMessage());
        }
    }
}
