package io.silentium;

import io.silentium.errorhandler.SilentError;
import io.silentium.errorhandler.SilentException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@FixMethodOrder(MethodSorters.JVM)
public class ProblemTest {
    @Mock
    private IOHelper mIOHelper;
    private Problem mProblem;

    @Before
    public void prepare() throws Exception {
        mIOHelper = PowerMockito.mock(IOHelper.class);
        PowerMockito.doNothing().when(mIOHelper, "printInt", Mockito.anyInt());
        PowerMockito.doNothing().when(mIOHelper, "printString", Mockito.anyString());
        mProblem = new Problem(mIOHelper);
    }

    @Test
    public void printMagicNumberWithInvalidArgumentTest() {
        for (int value = 0; value > -3; value--) {
            try {
                mProblem.printMagicNumber(value);
                Assert.fail("Should throw an exception");
            } catch (Exception e) {
                Assert.assertTrue(e.getMessage().contains(SilentError.ERR_INVALID_PARAMETER.name()));
            }
        }
    }

    @Test
    public void printMagicNumberTest() throws SilentException {
        mProblem.printMagicNumber(3);

        Mockito.verify(mIOHelper).printInt(1);
        Mockito.verify(mIOHelper).printInt(2);
        Mockito.verify(mIOHelper).printInt(3);
    }

    @Test
    public void printMagicNumberMultiples4Test() throws SilentException {
        for (int idx = 1; idx < 7; idx++) {
            verifyMultiples(idx * 4, idx, "Silentium");
        }

        // exclude value 28
        for (int idx = 7; idx < 14; idx++) {
            verifyMultiples(idx * 4, idx - 1, "Silentium");
        }
    }

    @Test
    public void printMagicNumberMultiples7Test() throws SilentException {
        for (int idx = 1; idx < 4; idx++) {
            verifyMultiples(idx * 7, idx, "IO");
        }

        // exclude value 28
        for (int idx = 4; idx < 8; idx++) {
            verifyMultiples(idx * 7, idx - 1, "IO");
        }
    }
    @Test
    public void printMagicNumberMultiples4And7Test() throws SilentException {
        for (int idx = 1; idx < 3; idx++) {
            verifyMultiples(idx * 28, idx, "SilentiumIO");
        }
    }

    private void verifyMultiples(int number, int times, String expected) throws SilentException {
        Mockito.reset(mIOHelper);
        mProblem.printMagicNumber(number);
        Mockito.verify(mIOHelper, Mockito.times(times)).printString(expected);
    }
}
