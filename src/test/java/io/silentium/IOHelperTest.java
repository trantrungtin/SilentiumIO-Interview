package io.silentium;

import io.silentium.errorhandler.SilentError;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.io.PrintStream;
import java.util.Scanner;

@RunWith(PowerMockRunner.class)
@FixMethodOrder(MethodSorters.JVM)
@PrepareForTest({Scanner.class, IOHelper.class, System.class})
public class IOHelperTest {
    private Scanner mScanner;
    private IOHelper mIOHelper;
    private PrintStream mPrintStream;

    @Before
    public void prepare() throws Exception {
        mPrintStream = PowerMockito.mock(PrintStream.class);
        PowerMockito.doNothing().when(mPrintStream, "println", Mockito.anyInt());
        PowerMockito.doNothing().when(mPrintStream, "println", Mockito.anyString());
        PowerMockito.mockStatic(System.class);
        Whitebox.setInternalState(System.class, "out", mPrintStream);

        mScanner = PowerMockito.mock(Scanner.class);
        mIOHelper = new IOHelper();
    }

    public void getConsoleNumberInvalidInput() {
        PowerMockito.when(mScanner.nextInt()).thenThrow(new Exception());
        try {
            mIOHelper.getConsoleNumber();
            Assert.fail("Should throw an exception");
        } catch (Exception e) {
            Assert.assertTrue(e.getMessage().contains(SilentError.ERR_READ_INPUT.name()));
        }
    }

    @Test
    public void getConsoleNumberTest() throws Exception {
        for (int value = -5; value < 5; value++) {
            mockScannerNextInt(value);
            int number = mIOHelper.getConsoleNumber();
            Assert.assertEquals(value, number);
        }
    }

    private void mockScannerNextInt(int returnValue) throws Exception {
        PowerMockito.when(mScanner.nextInt()).thenReturn(returnValue);
        Whitebox.setInternalState(mIOHelper, "mScanner", mScanner);
    }

    @Test
    public void printIntTest() {
        for (int value = -5; value < 5; value++) {
            mIOHelper.printInt(value);
            Mockito.verify(mPrintStream).println(value);
        }
    }

    @Test
    public void printStringTest() {
        mIOHelper.printString("SilentiumIO");
        Mockito.verify(mPrintStream).println("SilentiumIO");

        mIOHelper.printString("test");
        Mockito.verify(mPrintStream).println("test");
    }
}
